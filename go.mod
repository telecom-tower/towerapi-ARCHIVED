module gitlab.com/telecom-tower/towerapi

require (
	github.com/golang/protobuf v1.2.0
	golang.org/x/net v0.0.0-20181011144130-49bb7cea24b1
	google.golang.org/grpc v1.15.0
)
